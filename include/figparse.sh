version() {
    echo "$1, version ${VERSION}, GPLv3"
}

function figparse() {
    for SECTION in "${1}"
do
    if [ ! "${EPNUM}" ]; then 
	EPNUM=`sed -nr "/^\[$SECTION\]/ { :l /^ep(num)?(isode)?[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${NOTE}" ]; then 
	NOTE=`sed -nr "/^\[$SECTION\]/ { :l /^note[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${LINKDOC}" ]; then 
	LINKDOC=`sed -nr "/^\[$SECTION\]/ { :l /^linkdoc[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${HTEMPL}" ]; then 
	HTEMPL=`sed -nr "/^\[$SECTION\]/ { :l /^html[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${SRVPATH}" ]; then
	SRVPATH=`sed -nr "/^\[$SECTION\]/ { :l /^srvpath[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${SERVER}" ]; then 
	SERVER=`sed -nr "/^\[$SECTION\]/ { :l /^(web)?server[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${TARGET}" ]; then 
	TARGET=`sed -nr "/^\[$SECTION\]/ { :l /^target[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${SSHER}" ]; then 
	SSHER=`sed -nr "/^\[$SECTION\]/ { :l /^user[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${MEDIA}" ]; then 
	MEDIA=`sed -nr "/^\[$SECTION\]/ { :l /^media[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${CODEX}" ]; then 
	CODEX=`sed -nr "/^\[$SECTION\]/ { :l /^format(s)?[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${DELETE}" ]; then 
	DELETE=`sed -nr "/^\[$SECTION\]/ { :l /^delete[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${NOBUILD}" ]; then 
	NOBUILD=`sed -nr "/^\[$SECTION\]/ { :l /^no_build[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${SKIPHTML}" ]; then 
	SKIPHTML=`sed -nr "/^\[$SECTION\]/ { :l /^skip_html[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${PASS}" ]; then 
	PASS=`sed -nr "/^\[$SECTION\]/ { :l /^pass[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${AUTHOR}" ]; then 
	AUTHOR=`sed -nr "/^\[$SECTION\]/ { :l /^author[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${EMAIL}" ]; then 
	EMAIL=`sed -nr "/^\[$SECTION\]/ { :l /^email[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${PUBLISH}" ]; then 
	PUBLISH=`sed -nr "/^\[$SECTION\]/ { :l /^publish[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${COMMENT}" ]; then 
	COMMENT=`sed -nr "/^\[$SECTION\]/ { :l /^comment[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${TRACK}" ]; then 
	TRACK=`sed -nr "/^\[$SECTION\]/ { :l /^track[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${SHOW}" ]; then 
	SHOW=`sed -nr "/^\[$SECTION\]/ { :l /^show[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    if [ ! "${CATEGORY}" ]; then 
	CATEGORY=`sed -nr "/^\[$SECTION\]/ { :l /^category[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "${CONFIG}"`;fi
    done
}

function delete() {
/bin/rm -f *ml
echo "We don't remove media, so you'll have to tidy that up yourself."
}

function wildebeest() {
    # username,password123
    #CRED=$(cat "${PASS}" | gpg --decrypt)
    CREDRAW=$(cat "${PASS}" | gpg --decrypt)
    local D=$(echo "${CREDRAW}" | cut -b1) 
    #IFS=',' read -a CRED <<< $(echo "${CREDRAW}" | cut -f2,3 -d"${D}")
    # just use full user and pass plus delimiter
    CREDEN=$(echo $CREDRAW | cut -b2- )
}

function figlist() {
egrep '\[.*\]' "${CONFIG}" | tail -n +2
}
